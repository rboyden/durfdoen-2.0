---
titel: Gentse Biologische Kring
id: gbk
naam: Gentse Biologische Kring
verkorte_naam: Gentse Biologische Kring
konvent: fk
contact: praesidium@biologie-gent.be
themas:
  -  faculteit
---

$lang=nl$ 
De Gentse Biologische Kring is een kleine maar hechte vriendengroep rond de richting Biologie, die ervoor zorgt dat de studenten zich daar zo welkom mogelijk voelen.

Hun proefbuis-jeneverfuif is een graag geproefd concept waar elk jaar meer en meer smaak naar is. Als je je op maandagavond verveelt, spring dan eens binnen in the Porter House! Ze hebben daar namelijk hun wekelijkse clubavonden. 
$langend$ 
$lang=en$ 
The ‘Gentse Biologische Kring’ is a small but close group of friends from the Bachelor/Master programme Biology. It makes sure that students feel as welcome as possible. 

Their test tube/jenever party is a happily tasted concept, year and year to the taste of more people. If you are bored on a Monday night, drop by the Porter House! This is where they have their weekly club nights. 
$langend$

