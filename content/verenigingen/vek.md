---
titel: Vlaamse Economische Kring
id: vek
naam: Vlaamse Economische Kring
verkorte_naam: Vlaamse Economische Kring
konvent: fk
website: http://www.vek.be/
contact: praeses@vek.be
themas:
  -  faculteit
---
$lang=nl$ 
Als sinds 1923 is de Vlaamse Economische Kring de studentenvereniging van de hele Faculteit Economie en Bedrijfskunde. Onze kalender staat bol van studentikoze activiteiten: van bierbowlen tot paintballen, van citytrips tot skireizen en van kroegentochten tot ons befaamd galabal.
Het centrum van onze vereniging is de Yucca, het enige studentencafé in Gent waar je aan zeer studentikoze prijzen een dik feestje kan bouwen of gewoon kan genieten van ons geweldig terras!
Naast deze dagelijkse activiteiten verzorgen wij ook de boekenverkoop, organiseren wij het tweedaags fantastisch straatvoetbaltornooi Student Street Soccer, en hebben wij een professionele recruitmentwerking die onder andere zorgt voor een enorm succesvolle Career Day. 
$langend$ 
$lang=en$ 
The VEK (Flemish Economic association) was founded in 1923. Since then, this association has grown into one of the largest and most prominent student associations in Ghent. The VEK uses as official colors green and white, this color combination can be found on the student ribbons of the members.

The official song of the VEK, the so-called 'VEK lied', was written by Koen Beeckman and Frank Fivez. The text is accompanied on the tones of the song 'Omdat ik Vlaming ben'.

The VEK is built around the Faculty of Economics and Business Administration where more than 6000 students study. An enthusiastic Praesidium is ready every year to offer students interesting activities and actions.

The Yucca is our regular café and is still operated by ourselves for our fellow students. Over the years it has become a real meeting place for a cozy chat and a good party.

In addition, we also provide good guidance for the students on their way to the labor market through our many recruitment events with our own job fair, the Career Day.

In short: the VEK is always ready for the students at the Faculty of Economics and Business Administration and tries to make student life a pleasant experience for them. All this with a mix of several student-like activities. All our members will agree: at the VEK we will lift your student life to unseen heights!
$langend$
