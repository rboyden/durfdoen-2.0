---
titel: Moeder Theepot
id: theepot
naam: Moeder Theepot
verkorte_naam: Moeder Theepot
konvent: wvk
website: https://www.moedertheepot.com/
social:
  - platform: facebook
    link: https://facebook.com/moedertheepot
  - platform: twitter
    link: https://twitter.com/moedertheepot
  - platform: instagram
    link: https://instagram.com/moeder.theepot
themas:
  -  lifestyle
---

$lang=nl$ 
De warmste studentenvereniging van Gent. Iedereen is bij ons welkom voor een stukje taart, tasje thee en een gezellige babbel. Daarnaast organiseren we ook allerlei activiteiten. 
$langend$ 
$lang=en$ 
The warmest student association in Ghent. We welcome everyone for a slice of cake, a cup of tea and a friendly chat. Furthermore, we organise various activities. 
$langend$
