---
titel: IEEE SB
id: ieee
naam: IEEE SB
verkorte_naam: IEEE SB
konvent: wvk
website: http://www.student.ugent.be/ieee/
social: 
  - platform: facebook
    link: https://www.facebook.com/ieeesbgent
themas:
  - wetenschap
---

$lang=nl$ 
IEEE Student Branch Gent is de lokale afdeling van IEEE aan de UGent. We brengen hoofdzakelijk ingenieursstudenten elektrotechniek, computerwetenschappen en werktuigkunde samen, maar andere studenten met een interesse voor techniek zijn natuurlijk ook altijd welkom! We bieden een gevarieerd aanbod: van een technisch-wetenschappelijke evenementen (elektrische kart, lezingen,…) tot meer cultureel-sociale activiteiten (poolen, iemand?). Al onze evenementen geven een geweldige gelegenheid om studenten uit de andere disciplines te leren kennen! 
$langend$ 
$lang=en$ 
The IEEE Student Branch of Ghent has the purpose of representing IEEE at Ghent University. We unite students with an interest in electronic, electrical, mechanical or computer engineering. We offer a pleasant program which includes both technical-scientific and cultural-social activities. We have a mission to teach students about new technologies. Some activities which seem less educational (such as tastings) have the purpose of bringing students together for interdisciplinary networking. 
$langend$
