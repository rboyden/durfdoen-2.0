---
titel: Vlaamse Diergeneeskundige Kring
id: vdk
naam: Vlaamse Diergeneeskundige Kring
verkorte_naam: Vlaamse Diergeneeskundige Kring
konvent: fk
website: http://www.vlaamsdiergeneeskundigekring.be/
contact: VDK@fkserv.ugent.be
themas:
  -  faculteit
---
$lang=nl$ 
De Vlaamse Dierengeneeskundige Kring is de kring voor, je raadt het nooit, studenten Dierengeneeskunde. Doordat hun campus zo ver buiten Gent ligt, is er een hechte band tussen die studenten, een soort dierengeneeskundige familie. Naast hun super activiteiten organiseren ze elk jaar de EXPOVET, een vakbeurs voor dierenartsen in de Flanders Expo. 
$langend$ 
$lang=en$ 
The ‘Vlaamse Dierengeneeskundige Kring’ is the association for, you’ll never guess what, Veterinary Medicine students. Because of the fact that our campus is situated so far away from the city centre, there is a very close bond between those students, a kind of veterinary family. Besides their fantastic activities they organise an annual veterinary trade fair in Flanders Expo called ‘EXPOVET’. 
$langend$
