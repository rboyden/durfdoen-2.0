---
naam: Facultaire Studentenraad Dierengeneeskunde
verkorte_naam: DSR
titel: Studentenraad Dierengeneeskunde
id: fde
naam: Studentenraad Dierengeneeskunde
konvent: gsr
logo: /assets/sfeerfotos/LogoDSR.png
themas:
  - engagement
website: http://www.vlaamsdiergeneeskundigekring.be/Studentenraad.php
contact: dsr@student.ugent.be
themas: 
  - engagement
---
$lang=nl$ 
De Diergeneeskundige Studentenraad is een facultaire studentenraad die zich inzet voor alle studenten op onze prachtige Faculteit. Wij bestaan voornamelijk uit de jaarverantwoordelijken, die iedereen van zijn/haar jaar natuurlijk wel kent, en de andere studentenvertegenwoordigers (oftewel stuvers). 

Wat onze geweldige jaarverantwoordelijken allemaal voor ons doen (onze vragen beantwoorden, klachten doorgeven/oplossen, leswissels en natuurlijk de kliniekroosters) is alom bekend. Maar wat doen die andere stuvers nu eigenlijk? Zij zitten in de verschillende commissies en raden die onze Faculteit rijk is! Je kan het zo gek maar niet bedenken, of er is wel een commissie voor waar proffen, assistenten en studenten samen zitten om problemen aan te kaarten en oplossingen te bedenken. Ook in centrale raden zoals de Sociale Raad, de Gentse Studentenraad, de Onderwijsraad ed. is er plek voor Diergeneeskunde studenten en is het belangrijk dat wij, op ons eilandje in Merelbeke, niet worden vergeten! 
$langend$ 
$lang=en$ 
The Student Council Veterinary Medicine is a faculty student council that makes an effort for all the students at our beloved Faculty. We consist mainly of the ‘year representatives’, with whom pretty much everyone is familiar, and the other student representatives.

What our wonderful ‘year representatives’ do for us (answer our questions, address/solve problems, changes in the courses, the hospital schedule…) is well known. But what do the other student representatives actually do? They take part in the many commissions and councils of our Faculty. It’s hard to imagine a topic without a commission made out of professors, assistens and students discussing everything related to it. In the central councils (Social Council, Ghent Student Council, Education Council) there is a place for Veterinary Medicine students as well. All of this to make sure that we, on our island in Merelbeke, are borne in mind. 
$langend$
