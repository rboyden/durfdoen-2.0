---
titel: StuJardin
id: stujardin
naam: StuJardin
verkorte_naam: StuJardin
konvent: gsr
logo: /assets/sfeerfotos/stujardin.png
contact: http://stujardin@gmail.com/
website: http://student.ugent.be/stujardin/
social:
  - platform: facebook
    link: https://www.facebook.com/stujardin/
themas:
  -  groen
---

$lang=nl$ 
StuJardin teelt samen met studenten groenten, fruit en kruiden in de stad. Hiermee willen we studenten overtuigen dat stadstuinieren plezierig en noodzakelijk is. Het project is een stap naar een stedelijke samenleving die meer berust op zelfvoorziening en korte keten.
Het is immers duurzamer en milieuvriendelijker om je eigen lokaal geteelde groenten te consumeren dan voedsel te kopen dat lange afstanden heeft afgelegd voor het op je bord terecht komt.
Verder proberen we de consument bewust te maken van het productieproces door er in de praktijk mee bezig te zijn.
Concreet bestaat het project uit twee luiken. Enerzijds hebben we een permanente moestuin waarin we werken op vaste werkmomenten, maar waar iedereen steeds welkom is.
Anderzijds worden er in het tweede semester diverse groeiavonden georganiseerd. Dit jaar komt er naast een tweede editie van tuinieren op kot, ook een workshop rond composteren, een sprokkelwandeling rond insectenhotels en een kruidenwandeling.
Het project ontstond als een samenwerking tussen JNM (Jeugdbond voor Natuur en Milieu) en d'Urgent (de studentenvereniging voor een duurzame universiteit, toen nog UGent1010 genoemd). Later werd de GSR (Gentse Studentenraad) de derde partner in het project. StuJardin heeft ook steun gekregen van Kruiden Claus en Stad Gent. 
$langend$
$lang=en$ 
Together with students StuJardin cultivates vegetables, fruit and herbs in the city. Doing this we want to show students that urban gardening is both fun and necessary. The project is a step towards an urban society based more and more on self-sustainability and short supply chains. After all, it's more sustainable and eco-friendly to consume locally grown vegetables than buying food that has travelled a long way to get on your plate. Furthermore we try to make the consumer aware of the production process by actively being involved in it. Specifically, our project consists of two components. First of all we have a permanent garden, open to everyone, where we work at fixed hours. Secondly, we organise different ‘growth evenings’ during the second semester. This year the second edition of ‘gardening on kot’ (i.e. dorm/student apartment) will take place, together with a workshop composting, a ‘sprokkel’-walk around insect hotels and a ‘herbal walk’. This project was the result of the cooperation between the JNM (Youth Association for Nature and Environment) and d’Urgent (Student Association for a Durable University, back then still called UGent1010). Some time later the GSR (Ghent Student Council) became the third partner. StuJardin also received support from Kruiden Claus and Stad Gent.
$langend$
