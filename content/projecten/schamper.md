---
naam: Schamper
id: schamper
verkorte_naam: Schamper
konvent: schamper
website: https://www.schamper.ugent.be
contact: kernredactie@schamper.be
social:
  - platform: facebook
    link: https://www.facebook.com/schampergent/
  - platform: twitter
    link: https://twitter.com/Schamper
  - platform: instagram
    link: https://www.instagram.com/schamper_gent/
themas:
  - cultuur
---

$lang=nl$ 
Schamper is anders dan de facultaire verenigingen omdat ze niet allemaal hetzelfde studeren. Ze zijn anders dan de streekgebonden studentenclubs, omdat ze niet allemaal uit dezelfde contreien komen. Ze zijn anders dan politiek-filosofische verenigingen omdat ze niet allemaal dezelfde ideologische mening hebben. Maar ze zijn vooral anders, omdat ze kritische, schampere studentenjournalistiek brengen.
In de eerste plaats is Schamper trots op de journalistiek, literair en esthetisch hoogstaande pareltjes die tweewekelijks, in vijfduizendvoud, aan alle faculteiten en resto's afgeleverd worden.
Daarnaast wagen zij zich ook wel aan activiteiten zoals de ondertussen beruchte Schamperfuif of Schamperquiz. 
$langend$ 
$lang=en$ 
Schamper is different from the other faculty associations because its members do not follow the same programme. They differ from the regional student clubs because they do not come from the same place. They are different from the politico-philosophical association because they do not share the same ideological positions. But first and foremost they are different because they bring critical student journalism. Schamper is proud of the journalistic, literary and esthetical masterpieces it delivers biweekly to all the faculties and student restaurants. Besides all of this, they organise other, by now infamous, activities like the Schamperfuif, a big party, and the Schamperquiz. 
$langend$
